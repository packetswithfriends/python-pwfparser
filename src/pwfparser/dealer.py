"""Receives the PULL and deals to workers"""
from dataclasses import dataclass, field
import asyncio
import logging

import zmq.asyncio  # type: ignore  # Get rid of error: Cannot find implementation or library stub for module

from .baseclasses import PWFRunner, get_pull_socket, get_push_socket, get_pub_socket

LOGGER = logging.getLogger(__name__)


@dataclass
class PWFDealer(PWFRunner):
    """Deal stuff to workers, PUBlish results to anyone"""

    _pull: zmq.asyncio.Socket = field(default_factory=get_pull_socket, init=False, repr=False)
    _pub: zmq.asyncio.Socket = field(default_factory=get_pub_socket, init=False, repr=False)
    _work_socket: zmq.asyncio.Socket = field(default_factory=get_push_socket, init=False, repr=False)
    _results_socket: zmq.asyncio.Socket = field(default_factory=get_pull_socket, init=False, repr=False)

    async def setup(self) -> None:
        """Connect socket"""
        await super().setup()
        self._pull.bind(self.config["zmq"]["pull_socket"])
        self._pub.bind(self.config["zmq"]["pub_socket"])
        self._work_socket.bind(self.config["zmq"]["workers"]["work_socket"])
        self._results_socket.bind(self.config["zmq"]["workers"]["results_socket"])
        asyncio.create_task(self.packetpuller())
        asyncio.create_task(self.resultpuller())

    async def packetpuller(self) -> None:
        """Get packets, distribute to workers"""
        try:
            while self._exitcode is None:
                # Get the work data
                LOGGER.debug("Waiting for packet")
                workdata = await self._pull.recv_multipart()
                # Send it out to workers
                await self._work_socket.send_multipart(workdata)
        finally:
            if self._exitcode is None:
                self.quit()

    async def resultpuller(self) -> None:
        """Get results, publish them"""
        while self._exitcode is None:
            LOGGER.debug("Waiting for results")
            results = await self._results_socket.recv_multipart()
            LOGGER.debug("Got results {}".format(results))
            await self._pub.send_multipart([b"pwfparsed"] + results)
