"""Console entrypoints"""
import sys
import asyncio
import logging
from pathlib import Path

import click


from .dealer import PWFDealer
from .worker import PWFWorker
from .faker import PWFaker
from .dealer import LOGGER as DEALERLOGGER
from .worker import LOGGER as WORKERLOGGER
from .faker import LOGGER as FAKERLOGGER

LOGGER = logging.getLogger(__name__)


@click.command()
@click.option("-l", "--loglevel", help="Python log level, 10=DEBUG, 20=INFO, 30=WARNING, 40=CRITICAL", default=30)
@click.option("-v", "--verbose", count=True, help="Shorthand for info/debug loglevel (-v/-vv)")
@click.option("-id", "--workerid", help="Set the worker id", default="")
@click.option("--logpath", type=click.Path(exists=True), default=None)
@click.argument("configfile", type=click.Path(exists=True))
def worker_cli(configfile: Path, loglevel: int, verbose: int, workerid: str, logpath: Path) -> None:
    """Receive work items, parse, return results"""
    if verbose == 1:
        loglevel = 20
    if verbose >= 2:
        loglevel = 10
    logging.basicConfig(level=loglevel)
    LOGGER.setLevel(loglevel)
    WORKERLOGGER.setLevel(loglevel)

    service_instance = PWFWorker(Path(configfile))
    if workerid:
        service_instance.workerid = workerid
    if logpath:
        service_instance.pktsavedir = Path(logpath)

    exitcode = asyncio.get_event_loop().run_until_complete(service_instance.run())
    sys.exit(exitcode)


@click.command()
@click.option("-l", "--loglevel", help="Python log level, 10=DEBUG, 20=INFO, 30=WARNING, 40=CRITICAL", default=30)
@click.option("-v", "--verbose", count=True, help="Shorthand for info/debug loglevel (-v/-vv)")
@click.argument("configfile", type=click.Path(exists=True))
def dealer_cli(configfile: Path, loglevel: int, verbose: int) -> None:
    """Receive packets, deal to workers, publish results"""
    if verbose == 1:
        loglevel = 20
    if verbose >= 2:
        loglevel = 10
    logging.basicConfig(level=loglevel)
    LOGGER.setLevel(loglevel)
    DEALERLOGGER.setLevel(loglevel)

    service_instance = PWFDealer(Path(configfile))

    exitcode = asyncio.get_event_loop().run_until_complete(service_instance.run())
    sys.exit(exitcode)


@click.command()
@click.option("-l", "--loglevel", help="Python log level, 10=DEBUG, 20=INFO, 30=WARNING, 40=CRITICAL", default=30)
@click.option("-v", "--verbose", count=True, help="Shorthand for info/debug loglevel (-v/-vv)")
@click.argument("configfile", type=click.Path(exists=True))
def faker_cli(configfile: Path, loglevel: int, verbose: int) -> None:
    """Push fake data to dealer"""
    if verbose == 1:
        loglevel = 20
    if verbose >= 2:
        loglevel = 10
    logging.basicConfig(level=loglevel)
    LOGGER.setLevel(loglevel)
    FAKERLOGGER.setLevel(loglevel)

    service_instance = PWFaker(Path(configfile))

    exitcode = asyncio.get_event_loop().run_until_complete(service_instance.run())
    sys.exit(exitcode)
