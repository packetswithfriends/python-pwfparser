"""Baseclasses"""
from __future__ import annotations
from typing import MutableMapping, Union, Any
from pathlib import Path
from dataclasses import dataclass, field
import asyncio
import logging
import signal as posixsignal

import zmq  # type: ignore
import zmq.asyncio  # type: ignore
import tomlkit  # type: ignore

LOGGER = logging.getLogger(__name__)


def get_req_socket() -> zmq.asyncio.Socket:
    """Factory for a REQ socket"""
    return zmq.asyncio.Context.instance().socket(zmq.REQ)  # pylint: disable=E1101


def get_router_socket() -> zmq.asyncio.Socket:
    """Factory for a router socket"""
    return zmq.asyncio.Context.instance().socket(zmq.ROUTER)  # pylint: disable=E1101


def get_pull_socket() -> zmq.asyncio.Socket:
    """Factory for a pull socket"""
    return zmq.asyncio.Context.instance().socket(zmq.PULL)  # pylint: disable=E1101


def get_push_socket() -> zmq.asyncio.Socket:
    """Factory for a pull socket"""
    return zmq.asyncio.Context.instance().socket(zmq.PUSH)  # pylint: disable=E1101


def get_pub_socket() -> zmq.asyncio.Socket:
    """Factory for a pub socket"""
    return zmq.asyncio.Context.instance().socket(zmq.PUB)  # pylint: disable=E1101


@dataclass
class PWFRunner:
    """Base running service.

    shamelessly stolen from https://gitlab.com/advian-oss/python-datastreamservicelib/
    """

    configpath: Path
    config: MutableMapping[str, Any] = field(init=False, default_factory=dict)

    _exitcode: Union[None, int] = field(init=False, default=None)
    _quitevent: asyncio.Event = field(init=False, default_factory=asyncio.Event, repr=False)

    async def setup(self) -> None:
        """Set things up"""
        self.hook_signals()
        self.reload()

    async def teardown(self) -> None:
        """Called once by the run method before exiting"""

    def reload(self) -> None:
        """Called whenever the service is told to reload (usually via SIGHUP), setup might want to call this"""
        with self.configpath.open("rt", encoding="utf-8") as filepntr:
            self.config = tomlkit.parse(filepntr.read())

    def quit(self, exitcode: int = 0) -> None:
        """set the exitcode which as side-effect will tell the run method to do teardown and finally exit"""
        LOGGER.debug("called with code={}".format(exitcode))
        self._exitcode = exitcode
        self._quitevent.set()

    def hook_signals(self) -> None:
        """Hook handlers for signals"""
        loop = asyncio.get_event_loop()
        loop.add_signal_handler(posixsignal.SIGINT, self.quit)
        loop.add_signal_handler(posixsignal.SIGTERM, self.quit)
        try:
            loop.add_signal_handler(posixsignal.SIGHUP, self.reload)
        except AttributeError:
            # Windows does not implement all signals
            pass

    async def run(self) -> int:
        """Main entrypoint, should be called with asyncio.get_event_loop().run_until_complete()"""
        await self.setup()
        await self._quitevent.wait()
        if self._exitcode is None:
            LOGGER.error("Got quitevent but exitcode is not set")
            self._exitcode = 1
        await self.teardown()
        return self._exitcode
