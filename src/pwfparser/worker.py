"""Parser stuff"""
from typing import Dict, Any, Optional
from dataclasses import dataclass, field
from pathlib import Path
import datetime
import asyncio
import logging
import json
import uuid

import scapy.layers.dot11  # type: ignore
import zmq.asyncio  # type: ignore  # Get rid of error: Cannot find implementation or library stub for module

from .baseclasses import PWFRunner, get_pull_socket, get_push_socket

LOGGER = logging.getLogger(__name__)


def get_uuid_str() -> str:
    """what it says on the tin"""
    return str(uuid.uuid4())


@dataclass
class PWFWorker(PWFRunner):
    """Work on stuff, return results to dealer"""

    workerid: str = field(default_factory=get_uuid_str)
    pktsavedir: Optional[Path] = field(default=None)
    _work_socket: zmq.asyncio.Socket = field(default_factory=get_pull_socket, init=False, repr=False)
    _results_socket: zmq.asyncio.Socket = field(default_factory=get_push_socket, init=False, repr=False)

    async def setup(self) -> None:
        """Connect socket"""
        await super().setup()
        self._work_socket.connect(self.config["zmq"]["work_socket"])
        self._results_socket.connect(self.config["zmq"]["results_socket"])
        asyncio.create_task(self.processor())

    async def processor(self) -> None:
        """Process the incoming work item"""
        try:
            while self._exitcode is None:
                LOGGER.debug("Waiting for work")
                protocolv, node, interface, datalink, packet = await self._work_socket.recv_multipart()
                reply = {
                    "workerid": self.workerid,
                    "protocolv": ord(protocolv),
                    "nodename": node.decode("utf-8"),
                    "ifacename": interface.decode("utf-8"),
                    "datalink": datalink.decode("utf-8"),
                }
                if datalink == b"FakeLink":
                    pass
                if datalink == b"radiotap":
                    reply = PWFWorker.parse_radiotap(packet, reply)
                    if "type" not in reply:
                        # Could not figure out what to do with the packet
                        continue

                LOGGER.debug("Sending reply: {}".format(reply))
                await self._results_socket.send_multipart([b"\x01", json.dumps(reply).encode("utf-8")])
                if self.pktsavedir:
                    try:
                        now = datetime.datetime.utcnow()
                        fpath = self.pktsavedir / "{}_{}.bin".format(self.workerid, now.isoformat())
                        with open(str(fpath), "wb") as fpntr:
                            fpntr.write(packet)
                    except OSError:
                        LOGGER.exception("Could not log packet")
        finally:
            if self._exitcode is None:
                self.quit()

    @classmethod
    def parse_radiotap(cls, packet: bytes, reply: Dict[str, Any]) -> Dict[str, Any]:
        """Parse radiotap packet"""
        try:
            parsed = scapy.layers.dot11.RadioTap(packet)
            reply["chfreq"] = parsed.ChannelFrequency
            if parsed.haslayer(scapy.layers.dot11.Dot11Beacon):
                reply.update(
                    {
                        "type": "wifibeacon",
                        "ssid": parsed.info.decode("utf-8", errors="replace"),
                        "caps": parsed.sprintf("{Dot11Beacon:%Dot11Beacon.cap%}{Dot11ProbeResp:%Dot11ProbeResp.cap%}"),
                        "mac": parsed.addr2,
                    }
                )
            if parsed.haslayer(scapy.layers.dot11.Dot11ProbeReq):
                reply.update(
                    {"type": "wifiprobe", "ssid": parsed.info.decode("utf-8", errors="replace"), "mac": parsed.addr2,}
                )
        except Exception as exc:  # pylint: disable=W0703
            LOGGER.exception("Packet parse failed")
            reply["parsefail"] = str(exc)
        return reply
