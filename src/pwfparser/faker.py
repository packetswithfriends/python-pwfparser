"""Create fake packages for testing"""
from dataclasses import dataclass, field
import asyncio
import logging
import random

import zmq.asyncio  # type: ignore  # Get rid of error: Cannot find implementation or library stub for module

from .baseclasses import PWFRunner, get_push_socket

LOGGER = logging.getLogger(__name__)


@dataclass
class PWFaker(PWFRunner):
    """Fake packet sender, for testing workers"""

    _push: zmq.asyncio.Socket = field(default_factory=get_push_socket, init=False, repr=False)

    async def setup(self) -> None:
        """Connect socket"""
        await super().setup()
        self._push.connect(self.config["zmq"]["push_socket"])
        asyncio.create_task(self.packetpusher())

    async def packetpusher(self) -> None:
        """Push fake packets to the dealer"""
        try:
            while self._exitcode is None:
                ver = b"\x01"
                nodename = "fakenode-{}".format(random.randint(1, 30)).encode("utf-8")  # nosec
                ifacename = "fakemon{}".format(random.randint(100, 130)).encode("utf-8")  # nosec
                datalink = b"FakeLink"
                packet = b""
                await self._push.send_multipart([ver, nodename, ifacename, datalink, packet])
                await asyncio.sleep(0.5)
        finally:
            if self._exitcode is None:
                self.quit()
