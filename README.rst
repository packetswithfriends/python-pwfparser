=========
pwfparser
=========

PWF packet parser

Usage
-----

Preparations
^^^^^^^^^^^^

#. Install Poetry (see below)
#. Create virtualenv (see below)
#. Checkout this repo
#. run "poetry install"

Running
^^^^^^^

#. Start one "pwfdealer" ("pwfdealer dealer_config.toml")
#. Start as many workers as you want ("pwfworker worker_config.toml")
#. Profit ? Now other processes can subscribe to the parsed data.


Development
-----------

TLDR:

- Create and activate a Python 3.8 virtualenv (assuming virtualenvwrapper)::

    mkvirtualenv -p `which python3.8` my_virtualenv

- Init your repo::

    git init
    git add .
    git commit -m 'Cookiecutter stubs'
    git push origin master

- change to a branch::

    git checkout -b my_branch

- install Poetry: https://python-poetry.org/docs/#installation
- Install project deps and pre-commit hooks::

    poetry install
    pre-commit install
    pre-commit run --all-files

- Ready to go.

Remember to activate your virtualenv whenever working on the repo, this is needed
because pylint and mypy pre-commit hooks use the "system" python for now (because reasons).
